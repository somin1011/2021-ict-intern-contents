package javastory.club.stage1.step3;

import java.util.Arrays;

import javastory.club.stage1.step1.TravelClub;

public class ClubCoordinator {
	//
	private int capacity = 4; // constants not variable.  // 값이 변하지 않는 상수로 지정 -->
	private int nextIndex;
	private TravelClub[] clubs;   // 배열이기 때문에 임의로 크기를 지정해줘야함  //  storage대신에 배열로 데이터 저장 공간을 만듬

	public ClubCoordinator() {
		//
		this.nextIndex = 0;
		this.clubs = new TravelClub[capacity];
	}

	public boolean hasClubs() {
		//
		return nextIndex != 0;
	}

	// 여행클럽 존재여부 체크하기
	public boolean exist(String name) {
		//
		for (int i = 0; i < nextIndex; i++) {
			if (clubs[i] != null && clubs[i].getName().equals(name)) {
				return true;
			}
		}
		return false;
	}

	// 잘 이해가 안가는부분
	public String register(TravelClub newClub) {
		//
		if (this.exist(newClub.getName())) {
			return null;     // ????
		}

		// increase size
		if (nextIndex == clubs.length) {
			clubs = Arrays.copyOf(clubs, nextIndex + capacity);   //?????
		}

		clubs[nextIndex++] = newClub;

		return newClub.getName();
	}

	public TravelClub find(String name) {
		//
		for (int i = 0; i < nextIndex; i++) {
			if (clubs[i].getName().equals(name)) {
				return clubs[i];
			}
		}
		return null;
	}

	public TravelClub[] findAll() {
		//
		return Arrays.copyOf(clubs, nextIndex);    // clubs를  nextIndex 크기로 복사한 것을 리턴
	}

	public void modify(String name, String intro) {
		//
		if (!this.exist(name)) {
			return;
		}

		TravelClub club = this.find(name);
		club.setIntro(intro);
	}

	public void remove(String name) {
		//
		for (int i = 0; i < nextIndex; i++) {
			if (clubs[i] != null && clubs[i].getName().equals(name)) {
				clubs[i] = null;
				removeBlankInClubs(i);
				break;
			}
		}
	}

	private void removeBlankInClubs(int blankIndex) {
		//
		int lastIndex = nextIndex - 1;
		if (blankIndex == lastIndex) {
			return;
		}

		for (int i = blankIndex; i < lastIndex; i++) {
			clubs[i] = clubs[i + 1];
		}

		clubs[lastIndex] = null;
		nextIndex--;
	}
}
